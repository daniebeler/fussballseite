let urlTeams = "https://api.football-data.org/v2/competitions/2002/teams";

let token = "819babcd7902454f930c154272296d78"

fetch(urlTeams, {
    method:"GET",
    headers: {
        "x-auth-token": token
    }
})
.then(Response => Response.json())
.then(function (data){
    let html = "";
    data.teams.forEach(element => {
        html += "<div class ='col-4 holder'>";
        html += "<div class='inner-holder'>";
        html += "<div class='row justify-content-center align-items-center img-holder'>";
        html += "<img src='" + element.crestUrl + "'/></div>";
        html += "<div class='text-center pt-2 pb-2'>" + element.name + "</div>";
        html += "<div class='overlay d-flex align-items-center justify-content-center'><div>";
        html += "<div class='row txt-overlay justify-content-center'><h3>" + element.tla + "</h3></div>";
        html += "<div class='row txt-overlay justify-content-center'><a href='" + element.website + "'>" + element.website + "</a></div>";
        html += "</div></div></div></div>";
    });
    document.getElementById("teams").innerHTML = html;
})